﻿using MatrixLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestMatrix
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodMultiplicationByNumber1()
        {
            /*Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 6;
            matrix1[0, 1] = 2;
            matrix1[1, 0] = -5;
            matrix1[1, 1] = 3;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 12.0;
            test[0, 1] = 4.0;
            test[1, 0] = -10.0;
            test[1, 1] = 6.0;
            Matrix matrix3 = matrix1 * 2;
            //matrix3.Equals(test);
            Assert.AreEqual(matrix3, test);*/

            Random rnd = new Random();
            Matrix matrixA = new Matrix(2, 2);
            Matrix test = new Matrix(2, 2);
            Matrix matrixC = matrixA * 3;
            int num = 3;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrixA[i, j] = rnd.Next();
                    // test[i, j] = matrixA[i, j] * num;
                    test[i, j] = matrixC[i, j] / num;
                }
            }
            matrixA.Equals(test); 

        }
        [TestMethod]
        public void TestMethodMultiplicationByNumber2()
        {
            Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 3;
            matrix1[0, 1] = 5;
            matrix1[1, 0] = -7;
            matrix1[1, 1] = 9;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 6.0;
            test[0, 1] = 10.0;
            test[1, 0] = -14.0;
            test[1, 1] = 18.0;
            Matrix matrix3 = matrix1 * 2;
            //matrix3.Equals(test);
            Assert.AreEqual(matrix3, test);
        }
        [TestMethod]
        public void TestMethodMultiplicationByNumber3()
        {
            Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 3;
            matrix1[0, 1] = 5;
            matrix1[1, 0] = -7;
            matrix1[1, 1] = 9;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = -9.0;
            test[0, 1] = -15.0;
            test[1, 0] = 21.0;
            test[1, 1] = -27.0;
            Matrix matrix3 = matrix1 * -3;
            //matrix3.Equals(test);
            Assert.AreEqual(matrix3, test);
        }

        [TestMethod]
        public void TestMethodComposition1()
        {
           /* Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 3;
            matrix1[0, 1] = 5;
            matrix1[1, 0] = -7;
            matrix1[1, 1] = 9;
            Matrix matrix2 = new Matrix(2, 2);
            matrix2[0, 0] = 1;
            matrix2[0, 1] = -5;
            matrix2[1, 0] = 2;
            matrix2[1, 1] = 3;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 13.0;
            test[0, 1] = 0.0;
            test[1, 0] = 11.0;
            test[1, 1] = 62.0;
            Matrix matrix3 = matrix1 * matrix2;
            matrix3.Equals(test);
            //Assert.AreEqual(matrix3, test);*/

            Random rnd = new Random();
            Matrix matrixA = new Matrix(2, 2);
            Matrix matrixB = new Matrix(2, 2);
            Matrix matrixB1 = new Matrix(2, 2);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrixA[i, j] = rnd.Next();
                    matrixB[i, j] = rnd.Next();
                }
            }
            Matrix matrixC = matrixA * matrixB;
            Matrix test = new Matrix(2, 2);
            for ( int i = 0; i < 2; i++)
            {
                for ( int j = 0; j < 2; j++)
                {
                    matrixB1[i, j] = matrixB[j, i];
                }
            }
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    double sum = 0;
                    for (int r = 0; r < 2; r++)
                        sum += matrixC[i, r] * matrixB1[r, i];
                    test[i, j] = sum;
                }
            }
            matrixA.Equals(test);
        }

        [TestMethod]
        public void TestMethodComposition2()
        {
            Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 0;
            matrix1[0, 1] = 11;
            matrix1[1, 0] = -2;
            matrix1[1, 1] = 9;
            Matrix matrix2 = new Matrix(2, 2);
            matrix2[0, 0] = 4;
            matrix2[0, 1] = -5;
            matrix2[1, 0] = 2;
            matrix2[1, 1] = 3;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 22.0;
            test[0, 1] = 33.0;
            test[1, 0] = 10.0;
            test[1, 1] = 37.0;
            Matrix matrix3 = matrix1 * matrix2;
            matrix3.Equals(test);
            //Assert.AreEqual(matrix3, test);
        }

        [TestMethod]
        public void TestMethodComposition3()
        {
            Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 13;
            matrix1[0, 1] = -5;
            matrix1[1, 0] = 2;
            matrix1[1, 1] = 5;
            Matrix matrix2 = new Matrix(2, 2);
            matrix2[0, 0] = 10;
            matrix2[0, 1] = 1;
            matrix2[1, 0] = 6;
            matrix2[1, 1] = -3;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 100.0;
            test[0, 1] = 28.0;
            test[1, 0] = 50.0;
            test[1, 1] = -13.0;
            Matrix matrix3 = matrix1 * matrix2;
            matrix3.Equals(test);
            //Assert.AreEqual(matrix3, test);
        }

        [TestMethod]
        public void TestMethodComposition4()
        {
            Matrix matrix1 = new Matrix(2, 2);
            matrix1[0, 0] = 10;
            matrix1[0, 1] = 5;
            matrix1[1, 0] = -7;
            matrix1[1, 1] = 3;
            Matrix matrix2 = new Matrix(2, 2);
            matrix2[0, 0] = 11;
            matrix2[0, 1] = 5;
            matrix2[1, 0] = 4;
            matrix2[1, 1] = 3;
            Matrix test = new Matrix(2, 2);
            test[0, 0] = 130.0;
            test[0, 1] = 65.0;
            test[1, 0] = -65.0;
            test[1, 1] = -26.0;
            Matrix matrix3 = matrix1 * matrix2;
            matrix3.Equals(test);
            //Assert.AreEqual(matrix3, test);
        }
    }
}
