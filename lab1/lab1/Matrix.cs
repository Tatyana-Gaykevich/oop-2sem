﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLib
{
    public class Matrix
    {

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            Matrix parametr = (Matrix)obj;

            if (parametr.matrixColumn != this.matrixColumn || parametr.matrixRow != this.matrixRow)
                return false;

            for (int i = 0; i < this.matrixRow; i++)
                for (int j = 0; j < this.matrixColumn; j++)
                    if (this[i, j] != parametr[i, j])
                        return false;

            return true;

        }

        double[,] matrix;

        public int matrixRow
        {
            get { return matrix.GetLength(0); }
        }

        public int matrixColumn
        {
            get { return matrix.GetLength(1); }
        }

        public Matrix(int row, int column)
        {
            matrix = new double[row, column];
        }
        //Перегрузка индексатора для обращения к элементу матрицы
        // как к элементу двумерного массива
        public double this[int i, int j]
        {
            get { return matrix[i, j]; }
            set { matrix[i, j] = value; }
        }
        //Умножение на число
        public static Matrix operator *(Matrix m, int value)
        {
            Matrix mat = new Matrix(m.matrixRow, m.matrixColumn);
            for (int i = 0; i < m.matrixRow; i++)
                for (int j = 0; j < m.matrixColumn; j++)
                    mat[i, j] = m[i, j] * value;
            return mat;
        }
        // Произведение матриц
        public static Matrix operator *(Matrix first, Matrix second)
        {
            if (first.matrixRow == second.matrixColumn && first.matrixColumn == second.matrixRow)
            {
                Matrix matr = new Matrix(first.matrixRow, first.matrixColumn);
                for (int i = 0; i < first.matrixRow; i++)
                {
                    for (int j = 0; j < second.matrixColumn; j++)
                    {
                        double sum = 0;
                        for (int r = 0; r < first.matrixColumn; r++)
                            sum += first[i, r] * second[r, i];
                        matr[i, j] = sum;
                    }
                }
                return matr;
            }
            else
            {
                throw new Exception("Невозможно перемножить матрицы.");
            }
        }
        //Вывод
        public void PrintMatrix()
        {
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    Console.Write("  {0}  ", this[i, j]);

                }
                Console.Write("\n");
            }
        }
    }
}