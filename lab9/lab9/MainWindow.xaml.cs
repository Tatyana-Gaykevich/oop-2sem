﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace Lab9
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //private void BInt_Click(object sender, RoutedEventArgs e)
        //{
        //    if (new OpenFileDialog() is var dialog && dialog.ShowDialog() == true) 
        //        label.Content = Regex.Split(File.ReadAllText(dialog.FileName).ToLower(), @"[\s]+")
        //        .Select(str => Int32.Parse(str)).Sum(digit => digit % 2 == 0 ? digit : 0).ToString();
        //}
        private void BDouble_Click(object sender, RoutedEventArgs e)
        {
            if (new OpenFileDialog() is var dialog && dialog.ShowDialog() == true)
                label.Content = Regex.Split(File.ReadAllText(dialog.FileName).ToLower(), @"[\s]+")
                .Select(str => Double.Parse(str)).Sum(digit => digit % 2 == 0 ? digit : 0).ToString();
        }
    }
}
