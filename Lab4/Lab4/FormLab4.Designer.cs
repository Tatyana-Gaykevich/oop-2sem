﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Lab4
{
    public class FormLab4 : Form
    {
        private SplitContainer splitContainer1;
        private SplitContainer splitContainer2;
        private SplitContainer splitContainer3;
        private SplitContainer splitContainer4;
        private RichTextBox richTextBox;
        private OpenFileDialog openFileDialog;
        private SaveFileDialog saveFileDialog;
        private Button buttonOpen;
        private Button buttonSave;
        private Button buttonCreate;
      
        private string file;

        public FormLab4()
        {
            Init();
        }

        private void LoadFile(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            file = openFileDialog.FileName;
            StreamReader streamReader = new StreamReader(file);
            richTextBox.Text = streamReader.ReadToEnd();
            streamReader.Close();
        }

        private void SaveFile(object sender, EventArgs e)
        {

            if (file != null && File.Exists(file))
            {
                StreamWriter streamWriter = new StreamWriter(file, false);
                streamWriter.Write(richTextBox.Text);
                streamWriter.Close();
            }
        }


        private void Init()
        {
            splitContainer1 = new SplitContainer();
            splitContainer2 = new SplitContainer();
            splitContainer3 = new SplitContainer();
            splitContainer4 = new SplitContainer();
            richTextBox = new RichTextBox();
           
            buttonOpen = new Button();
            buttonSave = new Button();
            buttonCreate = new Button();
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "(*.txt)|*.txt";
            saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "(*.txt)|*.txt";
            saveFileDialog.DefaultExt = "(*.txt)|*.txt";
            saveFileDialog.CreatePrompt = true;
            saveFileDialog.AddExtension = true;
            ((ISupportInitialize)(splitContainer1)).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            ((ISupportInitialize)(splitContainer2)).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((ISupportInitialize)(splitContainer3)).BeginInit();
            splitContainer3.Panel1.SuspendLayout();
            splitContainer3.Panel2.SuspendLayout();
            splitContainer3.SuspendLayout();
            ((ISupportInitialize)(splitContainer4)).BeginInit();
            splitContainer4.Panel1.SuspendLayout();
            splitContainer4.Panel2.SuspendLayout();
            splitContainer4.SuspendLayout();
            SuspendLayout();
         
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.FixedPanel = FixedPanel.Panel2;
            splitContainer1.IsSplitterFixed = true;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Margin = new Padding(0);
            splitContainer1.Name = "splitContainer1";

            splitContainer1.Panel1.Controls.Add(richTextBox);

            splitContainer1.Panel2.Controls.Add(splitContainer2);
            splitContainer1.Size = new Size(800, 450);
            splitContainer1.SplitterDistance = 700;
            splitContainer1.TabIndex = 0;

            richTextBox.Dock = DockStyle.Fill;
            richTextBox.Location = new Point(0, 0);
            richTextBox.Name = "richTextBox";
            richTextBox.Size = new Size(700, 450);
            richTextBox.TabIndex = 0;
            richTextBox.Text = "";

            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.FixedPanel = FixedPanel.Panel1;
            splitContainer2.IsSplitterFixed = true;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            splitContainer2.Orientation = Orientation.Horizontal;

            splitContainer2.Panel1.Controls.Add(buttonOpen);

            splitContainer2.Panel2.Controls.Add(splitContainer3);
            splitContainer2.Size = new Size(100, 500);
            splitContainer2.SplitterDistance = 100;
            splitContainer2.TabIndex = 0;

            splitContainer3.Dock = DockStyle.Fill;
            splitContainer3.FixedPanel = FixedPanel.Panel1;
            splitContainer3.IsSplitterFixed = true;
            splitContainer3.Location = new Point(0, 0);
            splitContainer3.Name = "splitContainer3";
            splitContainer3.Orientation = Orientation.Horizontal;

            splitContainer3.Panel1.Controls.Add(buttonSave);

            splitContainer3.Panel2.Controls.Add(splitContainer4);
            splitContainer3.Size = new Size(96, 500);
            splitContainer3.SplitterDistance = 100;
            splitContainer3.TabIndex = 0;

            splitContainer4.Dock = DockStyle.Fill;
            splitContainer4.FixedPanel = FixedPanel.Panel1;
            splitContainer4.IsSplitterFixed = true;
            splitContainer4.Location = new Point(0, 0);
            splitContainer4.Name = "splitContainer4";
            splitContainer4.Orientation = Orientation.Horizontal;

            splitContainer4.Panel1.Controls.Add(buttonCreate);

            
            splitContainer4.Size = new Size(96, 500);
            splitContainer4.SplitterDistance = 100;
            splitContainer4.TabIndex = 0;

            buttonOpen.Dock = DockStyle.Fill;
            buttonOpen.Location = new Point(0, 0);
            buttonOpen.Name = "buttonLoad";
            buttonOpen.Size = new Size(100, 500);
            buttonOpen.TabIndex = 0;
            buttonOpen.Text = "Open";
            buttonOpen.UseVisualStyleBackColor = true;


            buttonOpen.Click += LoadFile;


            buttonSave.Dock = DockStyle.Fill;
            buttonSave.Location = new Point(0, 0);
            buttonSave.Name = "buttonSave";
            buttonSave.Size = new Size(96, 180);
            buttonSave.TabIndex = 0;
            buttonSave.Text = "Save";
            buttonSave.UseVisualStyleBackColor = true;


            buttonSave.Click += SaveFile;


            buttonCreate.Dock = DockStyle.Fill;
            buttonCreate.Location = new Point(0, 0);
            buttonCreate.Name = "buttonCreate";
            buttonCreate.Size = new Size(96, 150);
            buttonCreate.TabIndex = 0;
            buttonCreate.Text = "Create";
            buttonCreate.UseVisualStyleBackColor = true;


            buttonCreate.Click += (object sender, EventArgs e) => {
                saveFileDialog.ShowDialog();
                if (!File.Exists(saveFileDialog.FileName))
                    File.WriteAllText(saveFileDialog.FileName, richTextBox.Text);
                else
                    MessageBox.Show("File exists!");
            };

            //AutoScaleDimensions = new SizeF(6F, 13F);
            //AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitContainer1);
            Name = "FormLab4";
            Text = "Lab4";
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((ISupportInitialize)(splitContainer1)).EndInit();
            splitContainer1.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel2.ResumeLayout(false);
            ((ISupportInitialize)(splitContainer2)).EndInit();
            splitContainer2.ResumeLayout(false);
            splitContainer3.Panel1.ResumeLayout(false);
            splitContainer3.Panel2.ResumeLayout(false);
            ((ISupportInitialize)(splitContainer3)).EndInit();
            splitContainer3.ResumeLayout(false);
            splitContainer4.Panel1.ResumeLayout(false);
            splitContainer4.Panel2.ResumeLayout(false);
            ((ISupportInitialize)(splitContainer4)).EndInit();
            splitContainer4.ResumeLayout(false);
            ResumeLayout(false);
        }
    }
}
