﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library.Lab6;
using Library.Model;

namespace Tests
{
    [TestClass]
    public class ListTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(new Department("ФАИС"));
            departments.Add(new Department("ГЭФ"));

            int count = 2;

            Assert.AreEqual(departments.Count, count);
        }

        [TestMethod]
        public void TestMethod2()
        {
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(new Department("ФАИС"));
            departments.Add(new Department("ФАИС"));

            int count = 1;

            Assert.AreEqual(departments.Count, count);
        }

        [TestMethod]
        public void TestMethod3()
        {
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(new Department("ФАИС"));
            departments.Add(new Department("ГЭФ"));

            int count = 2;

            Assert.AreEqual(departments.Count, count);
        }

        [TestMethod]
        public void TestMethod4()
        {
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(new Department("ФАИС"));
            departments.Add(new Department("ФАИС"));

            int count = 1;

            Assert.AreEqual(departments.Count, count);
        }

        [TestMethod]
        public void TestMethod5()
        {
            Department a = new Department("asdas");
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(a);
            bool actual = departments.Remove(a);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestMethod6()
        {
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            bool actual = departments.Remove(new Department("asdas"));

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestMethod7()
        {
            Department a = new Department("asdas");
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(a);
            bool actual = departments.Contains(a);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestMethod8()
        {
            Department a = new Department("asdas");
            SuperLinkedList<Department> departments = new SuperLinkedList<Department>();
            departments.Add(a);
            bool actual = departments.Contains(new Department("as"));

            Assert.IsFalse(actual);
        }
    }
}
