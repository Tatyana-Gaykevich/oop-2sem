﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library.Model;

namespace Tests
{
    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            Department department = new Department("ФАИС");
            Department department1 = new Department("ФАИС");

            Assert.AreEqual(department1, department);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Department department = new Department("ФАИС");
            Department department1 = new Department("ФАИС1");

            Assert.AreNotEqual(department1, department);
        }

        [TestMethod]
        public void TestMethod3()
        {
            Equipment equipment = new Equipment("Стул");
            Equipment equipment1 = new Equipment("Стул");

            Assert.AreEqual(equipment, equipment1);
        }

        [TestMethod]
        public void TestMethod4()
        {
            Equipment equipment = new Equipment("Стол");
            Equipment equipment1 = new Equipment("Стол1");

            Assert.AreNotEqual(equipment, equipment1);
        }

        [TestMethod]
        public void TestMethod5()
        {
            Department department = new Department("ФАИС");
            Equipment equipment = new Equipment("Стол");
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment(10, 5, department, equipment, "Я");
            DepartmentsEquipment departmentsEquipment1 = new DepartmentsEquipment(10, 5, department, equipment, "Я");

            Assert.AreEqual(departmentsEquipment, departmentsEquipment1);
        }

        [TestMethod]
        public void TestMethod6()
        {
            Department department = new Department("ФАИС");
            Department department1 = new Department("ФАИС1");
            Equipment equipment = new Equipment("Стол");
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment(10, 5, department, equipment, "Я");
            DepartmentsEquipment departmentsEquipment1 = new DepartmentsEquipment(10, 5, department1, equipment, "Я");

            Assert.AreNotEqual(departmentsEquipment, departmentsEquipment1);
        }

        [TestMethod]
        public void TestMethod7()
        {
            Department department = new Department("ФАИС");
            Department department1 = new Department("ФАИС");
            Equipment equipment = new Equipment("Стол");
            Equipment equipment1 = new Equipment("Стол");
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment(10, 5, department, equipment1, "Я");
            DepartmentsEquipment departmentsEquipment1 = new DepartmentsEquipment(10, 5, department1, equipment, "Я");

            Assert.AreEqual(departmentsEquipment, departmentsEquipment1);
        }

        [TestMethod]
        public void TestMethod8()
        {
            Department department = new Department("ФАИС");
            Equipment equipment = new Equipment("Стол");
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment(9, 5, department, equipment, "Я");
            DepartmentsEquipment departmentsEquipment1 = new DepartmentsEquipment(10, 6, department, equipment, "Я");

            Assert.AreNotEqual(departmentsEquipment, departmentsEquipment1);
        }
    }
}
