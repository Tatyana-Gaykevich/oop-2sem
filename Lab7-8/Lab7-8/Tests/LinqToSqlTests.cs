﻿using Library.Lab7_8.BLL;
using Library.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class LinqToSqlTests
    {
        MainService service = new MainService("LINQTOSQL");

        [TestMethod]
        public void TestMethod1()
        {
            Department actual = service.GetDepartmentById(1);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Equipment actual = service.GetEquipmentById(1);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void TestMethod4()
        {
            int size = service.GetAllDepartmentsEquipment().Count;
            service.Delete(service.GetAllDepartmentsEquipment().First().Id);
            size--;
            Assert.AreEqual(size, service.GetAllDepartmentsEquipment().Count);
        }

        [TestMethod]
        public void TestMethod5()
        {
            int size = service.GetAllDepartmentsEquipment().Count;
            service.Delete(0);
            Assert.AreEqual(size, service.GetAllDepartmentsEquipment().Count);
        }

        [TestMethod]
        public void TestMethod6()
        {
            int size = service.GetAllDepartmentsEquipment().Count;
            service.Update(null);
            Assert.AreEqual(size, service.GetAllDepartmentsEquipment().Count);
        }

        [TestMethod]
        public void TestMethod7()
        {
            int size = service.GetAllDepartmentsEquipment().Count;
            service.Insert(null);
            Assert.AreEqual(size, service.GetAllDepartmentsEquipment().Count);
        }
    }
}
