﻿using Library.Lab7_8.BLL;
using Library.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8WF
{
    public partial class Form1 : Form
    {
        readonly MainService service = new MainService("LINQTOSQL");

        public Form1()
        {
            InitializeComponent();

            ElementsDataGridView.AllowUserToDeleteRows = false;
            ElementsDataGridView.AllowUserToAddRows = false;
            ElementsDataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            DepartmentComboBox.Items.AddRange(service.GetAllDepartments().ToArray());
            EquipmentComboBox.Items.AddRange(service.GetAllEquipments().ToArray());
        }

        private void GetAllBtn_Click(object sender, EventArgs e)
        {
            RefreshTable(service.GetAllDepartmentsEquipment());
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGridView.SelectedRows[0].DataBoundItem;
            service.Delete(departmentsEquipment.Id);
            RefreshTable(service.GetAllDepartmentsEquipment());
        }

        private void RefreshTable(List<DepartmentsEquipment> collection)
        {
            ElementsDataGridView.DataSource = collection;
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGridView.SelectedRows[0].DataBoundItem;
            if (departmentsEquipment != null)
            {
                try
                {
                    departmentsEquipment.Count = int.Parse(CountTextBox.Text);
                    departmentsEquipment.Price = decimal.Parse(PriceTextBox.Text);
                    departmentsEquipment.ResponsiblePerson = ResponsiblePersonTextBox.Text;
                    departmentsEquipment.DepartmentId = DepartmentComboBox.SelectedIndex + 1;
                    departmentsEquipment.EquipmentId = EquipmentComboBox.SelectedIndex + 1;
                    service.Update(departmentsEquipment);
                    RefreshTable(service.GetAllDepartmentsEquipment());
                }
                catch (Exception ex)
                {
                    /* Empty block */
                }
            }
        }

        private void CreateBtn_Click(object sender, EventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment();
            try
            {
                departmentsEquipment.Count = int.Parse(CountTextBox.Text);
                departmentsEquipment.Price = decimal.Parse(PriceTextBox.Text);
                departmentsEquipment.ResponsiblePerson = ResponsiblePersonTextBox.Text;
                departmentsEquipment.DepartmentId = DepartmentComboBox.SelectedIndex + 1;
                departmentsEquipment.EquipmentId = EquipmentComboBox.SelectedIndex + 1;
                service.Insert(departmentsEquipment);
                RefreshTable(service.GetAllDepartmentsEquipment());
            }
            catch
            {
                /* Empty block */
            }
        }

        private void Table_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGridView.SelectedRows[0].DataBoundItem;

            if (departmentsEquipment != null)
            {
                ResponsiblePersonTextBox.Text = departmentsEquipment.ResponsiblePerson;
                CountTextBox.Text = departmentsEquipment.Count.ToString();
                PriceTextBox.Text = departmentsEquipment.Price.ToString();
                DepartmentComboBox.SelectedItem = departmentsEquipment.Department;
                EquipmentComboBox.SelectedItem = departmentsEquipment.Equipment;
            }
        }

       
    }
}
