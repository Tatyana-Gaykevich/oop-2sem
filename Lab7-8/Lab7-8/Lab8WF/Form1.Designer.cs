﻿namespace Lab8WF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ElementsDataGridView = new System.Windows.Forms.DataGridView();
            this.GetAllBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.CreateBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ResponsiblePersonTextBox = new System.Windows.Forms.TextBox();
            this.CountTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PriceTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DepartmentComboBox = new System.Windows.Forms.ComboBox();
            this.EquipmentComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ElementsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ElementsDataGridView
            // 
            this.ElementsDataGridView.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ElementsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ElementsDataGridView.Location = new System.Drawing.Point(12, 12);
            this.ElementsDataGridView.Name = "ElementsDataGridView";
            this.ElementsDataGridView.Size = new System.Drawing.Size(640, 310);
            this.ElementsDataGridView.TabIndex = 0;
            this.ElementsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Table_CellClick);
            // 
            // GetAllBtn
            // 
            this.GetAllBtn.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.GetAllBtn.Location = new System.Drawing.Point(21, 345);
            this.GetAllBtn.Name = "GetAllBtn";
            this.GetAllBtn.Size = new System.Drawing.Size(140, 38);
            this.GetAllBtn.TabIndex = 1;
            this.GetAllBtn.Text = "Вывести список";
            this.GetAllBtn.UseVisualStyleBackColor = true;
            this.GetAllBtn.Click += new System.EventHandler(this.GetAllBtn_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.DeleteBtn.Location = new System.Drawing.Point(179, 345);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(132, 39);
            this.DeleteBtn.TabIndex = 2;
            this.DeleteBtn.Text = "Удалить элемент";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.UpdateBtn.Location = new System.Drawing.Point(327, 345);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(150, 38);
            this.UpdateBtn.TabIndex = 3;
            this.UpdateBtn.Text = "Изменить";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // CreateBtn
            // 
            this.CreateBtn.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.CreateBtn.Location = new System.Drawing.Point(496, 345);
            this.CreateBtn.Name = "CreateBtn";
            this.CreateBtn.Size = new System.Drawing.Size(133, 39);
            this.CreateBtn.TabIndex = 4;
            this.CreateBtn.Text = "Добавить";
            this.CreateBtn.UseVisualStyleBackColor = true;
            this.CreateBtn.Click += new System.EventHandler(this.CreateBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.label1.Location = new System.Drawing.Point(24, 409);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 31);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ответственное лицо:";
            // 
            // ResponsiblePersonTextBox
            // 
            this.ResponsiblePersonTextBox.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.ResponsiblePersonTextBox.Location = new System.Drawing.Point(266, 406);
            this.ResponsiblePersonTextBox.MaxLength = 50;
            this.ResponsiblePersonTextBox.Name = "ResponsiblePersonTextBox";
            this.ResponsiblePersonTextBox.Size = new System.Drawing.Size(211, 39);
            this.ResponsiblePersonTextBox.TabIndex = 6;
            // 
            // CountTextBox
            // 
            this.CountTextBox.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.CountTextBox.Location = new System.Drawing.Point(430, 450);
            this.CountTextBox.MaxLength = 4;
            this.CountTextBox.Name = "CountTextBox";
            this.CountTextBox.Size = new System.Drawing.Size(110, 39);
            this.CountTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.label2.Location = new System.Drawing.Point(24, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 31);
            this.label2.TabIndex = 7;
            this.label2.Text = "Стоимость:";
            // 
            // PriceTextBox
            // 
            this.PriceTextBox.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.PriceTextBox.Location = new System.Drawing.Point(167, 450);
            this.PriceTextBox.MaxLength = 4;
            this.PriceTextBox.Name = "PriceTextBox";
            this.PriceTextBox.Size = new System.Drawing.Size(110, 39);
            this.PriceTextBox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.label3.Location = new System.Drawing.Point(283, 453);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 31);
            this.label3.TabIndex = 9;
            this.label3.Text = "Количество:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.label4.Location = new System.Drawing.Point(24, 502);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 31);
            this.label4.TabIndex = 11;
            this.label4.Text = "Кафедра:";
            // 
            // DepartmentComboBox
            // 
            this.DepartmentComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.DepartmentComboBox.FormattingEnabled = true;
            this.DepartmentComboBox.Location = new System.Drawing.Point(141, 496);
            this.DepartmentComboBox.Name = "DepartmentComboBox";
            this.DepartmentComboBox.Size = new System.Drawing.Size(110, 37);
            this.DepartmentComboBox.TabIndex = 12;
            // 
            // EquipmentComboBox
            // 
            this.EquipmentComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.EquipmentComboBox.FormattingEnabled = true;
            this.EquipmentComboBox.Location = new System.Drawing.Point(443, 495);
            this.EquipmentComboBox.Name = "EquipmentComboBox";
            this.EquipmentComboBox.Size = new System.Drawing.Size(186, 37);
            this.EquipmentComboBox.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Sylfaen", 18F);
            this.label5.Location = new System.Drawing.Point(257, 498);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 31);
            this.label5.TabIndex = 13;
            this.label5.Text = "Оборудование:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(664, 565);
            this.Controls.Add(this.EquipmentComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DepartmentComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PriceTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CountTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ResponsiblePersonTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CreateBtn);
            this.Controls.Add(this.UpdateBtn);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.GetAllBtn);
            this.Controls.Add(this.ElementsDataGridView);
            this.Name = "Form1";
            this.Text = "Лабораторная работа 8";
            ((System.ComponentModel.ISupportInitialize)(this.ElementsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ElementsDataGridView;
        private System.Windows.Forms.Button GetAllBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.Button CreateBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ResponsiblePersonTextBox;
        private System.Windows.Forms.TextBox CountTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PriceTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox DepartmentComboBox;
        private System.Windows.Forms.ComboBox EquipmentComboBox;
        private System.Windows.Forms.Label label5;
    }
}

