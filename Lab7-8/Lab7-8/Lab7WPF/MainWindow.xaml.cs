﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Library.Lab7_8.BLL;
using Library.Model;

namespace Lab7WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly MainService service = new MainService("MSSQL");

        public MainWindow()
        {
            InitializeComponent();

            DepartmentComboBox.ItemsSource = service.GetAllDepartments().ToArray();
            EquipmentComboBox.ItemsSource = service.GetAllEquipments().ToArray();
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshTable(service.GetAllDepartmentsEquipment());
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGrid.SelectedItem;
            if (departmentsEquipment != null)
            {
                service.Delete(departmentsEquipment.Id);
                RefreshTable(service.GetAllDepartmentsEquipment());
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGrid.SelectedItem;
            if (departmentsEquipment != null)
            {
                try
                {
                    departmentsEquipment.Count = int.Parse(CountTextBox.Text);
                    departmentsEquipment.Price = decimal.Parse(PriceTextBox.Text);
                    departmentsEquipment.ResponsiblePerson = ResponsiblePersonTextBox.Text;
                    departmentsEquipment.DepartmentId = DepartmentComboBox.SelectedIndex + 1;
                    departmentsEquipment.EquipmentId = EquipmentComboBox.SelectedIndex + 1;
                    service.Update(departmentsEquipment);
                }
                catch (Exception ex)
                {
                    /* Empty block */
                }
                RefreshTable(service.GetAllDepartmentsEquipment());
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment();
            if (departmentsEquipment != null)
            {
                try
                {
                    departmentsEquipment.Count = int.Parse(CountTextBox.Text);
                    departmentsEquipment.Price = decimal.Parse(PriceTextBox.Text);
                    departmentsEquipment.ResponsiblePerson = ResponsiblePersonTextBox.Text;
                    departmentsEquipment.DepartmentId = DepartmentComboBox.SelectedIndex + 1;
                    departmentsEquipment.EquipmentId = EquipmentComboBox.SelectedIndex + 1;
                    service.Insert(departmentsEquipment);
                    RefreshTable(service.GetAllDepartmentsEquipment());
                }
                catch
                {
                    /* Empty block */
                }
            }
        }
        private void RefreshTable(List<DepartmentsEquipment> collection)
        {
            ElementsDataGrid.ItemsSource = collection;
        }
        private void ElementsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DepartmentsEquipment departmentsEquipment = (DepartmentsEquipment)ElementsDataGrid.SelectedItem;
            if (departmentsEquipment != null)
            {
                try
                {
                    CountTextBox.Text = departmentsEquipment.Count.ToString();
                    PriceTextBox.Text = departmentsEquipment.Price.ToString();
                    ResponsiblePersonTextBox.Text = departmentsEquipment.ResponsiblePerson;
                    DepartmentComboBox.SelectedItem = departmentsEquipment.Department;
                    EquipmentComboBox.SelectedItem = departmentsEquipment.Equipment;
                }
                catch
                {
                    /* Empty block */
                }
            }
        }
    }
}
