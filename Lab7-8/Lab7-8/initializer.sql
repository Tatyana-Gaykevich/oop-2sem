use oopDB;

DROP TABLE departmentsEquipment;
CREATE TABLE departmentsEquipment (
    Id              INT           IDENTITY (1, 1) PRIMARY KEY NOT NULL,
    DepartmentId          INT           NOT NULL,
    EquipmentId          INT NOT NULL,
    ResponsiblePerson   NVARCHAR (255) NOT NULL,
    Count    INT      NOT NULL,
    Price      DECIMAL      NOT NULL
);

DROP TABLE departments;
CREATE TABLE departments (
	Id               INT     PRIMARY KEY      IDENTITY (1, 1) NOT NULL,
	Name             NVARCHAR (50) NOT NULL,
)

DROP TABLE equipments;
CREATE TABLE equipments (
	Id               INT     PRIMARY KEY      IDENTITY (1, 1) NOT NULL,
	Name             NVARCHAR (50) NOT NULL,
)

ALTER TABLE departmentsEquipment
ADD FOREIGN KEY (DepartmentId) REFERENCES departments (Id);

ALTER TABLE departmentsEquipment
ADD FOREIGN KEY (EquipmentId) REFERENCES equipments (Id);

INSERT INTO departments (Name) VALUES ('ИП');
INSERT INTO departments (Name) VALUES ('ИТ');
INSERT INTO departments (Name) VALUES ('ИС');
INSERT INTO departments (Name) VALUES ('ПЭ');

INSERT INTO equipments (Name) VALUES ('Компьютер');
INSERT INTO equipments (Name) VALUES ('Осцилограф');
INSERT INTO equipments (Name) VALUES ('Вентилятор');

INSERT INTO departmentsEquipment (DepartmentId, EquipmentId, ResponsiblePerson, Count, Price) VALUES (1, 1, 'Лаборант',  5, 25);
INSERT INTO departmentsEquipment (DepartmentId, EquipmentId, ResponsiblePerson, Count, Price) VALUES (2, 2, 'Завхоз',  5, 25);
INSERT INTO departmentsEquipment (DepartmentId, EquipmentId, ResponsiblePerson, Count, Price) VALUES (3, 3, 'Техничка',  5, 25);