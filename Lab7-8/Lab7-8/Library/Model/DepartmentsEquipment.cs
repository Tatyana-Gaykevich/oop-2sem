﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;

namespace Library.Model
{
    /// <summary>
    /// Оборудование кафедры.
    /// </summary>
    [Table(Name = "departmentsEquipment")]
    public class DepartmentsEquipment
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Стоимость единицы оборудования.
        /// </summary>
        [Column(Name = "Price")]
        public decimal Price { get; set; }

        /// <summary>
        /// Количество.
        /// </summary>
        [Column(Name = "Count")]
        public int Count { get; set; }

        /// <summary>
        /// Кафедра.
        /// </summary>
        public Department Department { get; set; }

        /// <summary>
        /// Название оборудования.
        /// </summary>
        public Equipment Equipment { get; set; }

        /// <summary>
        /// Ответственное лицо.
        /// </summary>
        [Column(Name = "ResponsiblePerson")]
        public string ResponsiblePerson { get; set; }

        [Column(Name = "DepartmentId")]
        public int DepartmentId { get; set; }

        [Column(Name = "EquipmentId")]
        public int EquipmentId { get; set; }

        public DepartmentsEquipment(decimal price, int count, Department department, Equipment equipment, string responsiblePerson)
        {
            Price = price;
            Count = count;
            Department = department ?? throw new ArgumentNullException(nameof(department));
            Equipment = equipment ?? throw new ArgumentNullException(nameof(equipment));
            ResponsiblePerson = responsiblePerson ?? throw new ArgumentNullException(nameof(responsiblePerson));
            DepartmentId = department.Id;
            EquipmentId = equipment.Id;
        }

        public DepartmentsEquipment() { }

        public override bool Equals(object obj)
        {
            return obj is DepartmentsEquipment equipment &&
                   Price == equipment.Price &&
                   Count == equipment.Count &&
                   EqualityComparer<Department>.Default.Equals(Department, equipment.Department) &&
                   EqualityComparer<Equipment>.Default.Equals(Equipment, equipment.Equipment) &&
                   ResponsiblePerson == equipment.ResponsiblePerson;
        }

        public override int GetHashCode()
        {
            int hashCode = 421857730;
            hashCode = hashCode * -1521134295 + Price.GetHashCode();
            hashCode = hashCode * -1521134295 + Count.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Department>.Default.GetHashCode(Department);
            hashCode = hashCode * -1521134295 + EqualityComparer<Equipment>.Default.GetHashCode(Equipment);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ResponsiblePerson);
            return hashCode;
        }

        public object Clone()
        {
            return new DepartmentsEquipment()
            {
                Id = this.Id,
                DepartmentId = this.DepartmentId,
                EquipmentId = this.EquipmentId,
                ResponsiblePerson = this.ResponsiblePerson,
                Count = this.Count,
                Price = this.Price
            };
        }
    }
}
