﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Model
{
    /// <summary>
    /// Оборудование.
    /// </summary>
    [Table(Name = "equipments")]
    public class Equipment
    {
        /// <summary>
        /// Название.
        /// </summary>
        [Column(Name = "Name")]
        public string Name { get; set; }

        [Column(IsPrimaryKey = true, IsDbGenerated = true, Name = "Id")]
        public int Id { get; set; }

        public Equipment(string name)
        {
            Name = name;
        }

        public Equipment() { }
        public override bool Equals(object obj)
        {
            return obj is Equipment equipment &&
                   Name == equipment.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
