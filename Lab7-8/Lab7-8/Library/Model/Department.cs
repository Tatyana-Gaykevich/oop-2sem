﻿using System.Collections.Generic;
using System.Data.Linq.Mapping;

namespace Library.Model
{

    /// <summary>
    /// Кафедра.
    /// </summary>
    [Table(Name = "departments")]
    public class Department
    {
        /// <summary>
        /// Название.
        /// </summary>
        [Column(Name = "Name")]
        public string Name { get; set; }

        [Column(IsPrimaryKey = true, IsDbGenerated = true, Name = "Id")]
        public int Id { get; set; }

        public Department(string name)
        {
            Name = name;
        }

        public Department() { }

        public override bool Equals(object obj)
        {
            return obj is Department department &&
                   Name == department.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
