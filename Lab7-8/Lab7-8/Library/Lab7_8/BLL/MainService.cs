﻿using Library.Model;
using System.Collections.Generic;

namespace Library.Lab7_8.BLL
{
    public class MainService
    {
        private BaseFactory factory = null;

        public MainService(string name)
        {
            factory = BaseFactory.CreateFactory(name);
        }

        public List<Department> GetAllDepartments() => factory.DepartmentCrud.Select();
        public List<Equipment> GetAllEquipments() => factory.EquipmentCrud.Select();
        public List<DepartmentsEquipment> GetAllDepartmentsEquipment() => factory.DepartmentsEquipmentCrud.Select();

        public void Delete(int id)
        {
            if (id > 0)
            {
                factory.DepartmentsEquipmentCrud.Delete(id);
            }
        }

        public void Update(DepartmentsEquipment obj)
        {
            if (obj != null)
            {
                factory.DepartmentsEquipmentCrud.Update(obj);
            }
        }

        public void Insert(DepartmentsEquipment obj)
        {
            if (obj != null)
            {
                factory.DepartmentsEquipmentCrud.Insert(obj);
            }
        }

        public Department GetDepartmentById(int id)
        {
            if (id > 0)
            {
                return factory.DepartmentCrud.GetById(id);
            }
            return null;
        }

        public Equipment GetEquipmentById(int id)
        {
            if (id > 0)
            {
                return factory.EquipmentCrud.GetById(id);
            }
            return null;
        }
    }
}
