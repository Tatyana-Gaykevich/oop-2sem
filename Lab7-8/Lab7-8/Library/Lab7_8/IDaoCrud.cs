﻿using System.Collections.Generic;

namespace Library.Lab7_8
{
    public interface IDaoCrud<Entity>
    {
        List<Entity> Select();
        bool Insert(Entity o);
        bool Update(Entity o);
        bool Delete(int id);
    }
}
