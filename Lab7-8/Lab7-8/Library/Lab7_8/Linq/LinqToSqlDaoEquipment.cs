﻿using Library.Lab7_8;
using Library.Model;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace Library.Lab7_8.Linq
{
    public class LinqToSqlDaoEquipment : IEquipmentCrud
    {
        public Equipment GetById(int id)
        {
            Equipment equipment = null;

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                equipment = db.GetTable<Equipment>().Where(f => f.Id == id).FirstOrDefault();
            }

            return equipment;
        }

        public List<Equipment> Select()
        {
            List<Equipment> equipment = new List<Equipment>();

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                equipment = db.GetTable<Equipment>().ToList();
            }

            return equipment;
        }
    }
}