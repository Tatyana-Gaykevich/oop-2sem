﻿using Library.Lab7_8;
using Library.Model;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace Library.Lab7_8.Linq
{
    public class LinqToSqlDaoDepartmentsEquipment : IDaoCrud<DepartmentsEquipment>
    {
        public bool Delete(int id)
        {
            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                DepartmentsEquipment departmentsEquipment = db.GetTable<DepartmentsEquipment>().Where(f => f.Id == id).FirstOrDefault();

                db.GetTable<DepartmentsEquipment>().DeleteOnSubmit(departmentsEquipment);
                db.SubmitChanges();
            }

            return true;
        }

        public bool Insert(DepartmentsEquipment o)
        {
            if (o == null) return false;

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                DepartmentsEquipment departmentsEquipment = db.GetTable<DepartmentsEquipment>().Where(f => f.Id == o.Id).FirstOrDefault();

                db.GetTable<DepartmentsEquipment>().InsertOnSubmit(o);
                db.SubmitChanges();
            }

            return true;
        }

        public List<DepartmentsEquipment> Select()
        {
            List<DepartmentsEquipment> departmentsEquipment = new List<DepartmentsEquipment>();
            LinqToSqlDaoDepartment linqToSqlDaoDepartment = new LinqToSqlDaoDepartment();
            LinqToSqlDaoEquipment linqToSqlDaoEquipment = new LinqToSqlDaoEquipment();

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                departmentsEquipment = db.GetTable<DepartmentsEquipment>().ToList();
                foreach (var item in departmentsEquipment)
                {
                    Department department = linqToSqlDaoDepartment.GetById(item.DepartmentId);
                    Equipment equipment = linqToSqlDaoEquipment.GetById(item.EquipmentId);
                    item.Department = department;
                    item.Equipment = equipment;
                }
            }

            return departmentsEquipment;
        }

        public bool Update(DepartmentsEquipment o)
        {
            if (o == null) return false;

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                DepartmentsEquipment departmentsEquipment = db.GetTable<DepartmentsEquipment>().Where(f => f.Id == o.Id).Single();

                if (departmentsEquipment == null) return false;

                departmentsEquipment.DepartmentId = o.DepartmentId;
                departmentsEquipment.EquipmentId = o.EquipmentId;

                db.SubmitChanges();
            }

            return true;
        }
    }
}