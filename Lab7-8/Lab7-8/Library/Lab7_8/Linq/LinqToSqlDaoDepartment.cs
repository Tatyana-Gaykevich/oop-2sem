﻿using Library.Model;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace Library.Lab7_8.Linq
{
    public class LinqToSqlDaoDepartment : IDepartmentCrud
    {
        public Department GetById(int id)
        {
            Department department = null;

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                department = db.GetTable<Department>().Where(f => f.Id == id).FirstOrDefault();
            }

            return department;
        }

        public List<Department> Select()
        {
            List<Department> department = new List<Department>();

            using (DataContext db = new DataContext(DBConnection.Instance.ConnectionString))
            {
                department = db.GetTable<Department>().ToList();
            }

            return department;
        }
    }
}