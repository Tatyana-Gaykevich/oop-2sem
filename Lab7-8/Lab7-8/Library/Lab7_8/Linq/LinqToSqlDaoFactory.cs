﻿using Library.Model;

namespace Library.Lab7_8.Linq
{
    public class LinqToSqlDaoFactory : BaseFactory
    {
        private IDepartmentCrud daoDepartment = null;
        private IEquipmentCrud daoEquipment = null;
        private IDaoCrud<DepartmentsEquipment> daoDepartmentEquipment = null;
        public override IDepartmentCrud DepartmentCrud { get => daoDepartment ?? (daoDepartment = new LinqToSqlDaoDepartment()); }
        public override IEquipmentCrud EquipmentCrud { get => daoEquipment ?? (daoEquipment = new LinqToSqlDaoEquipment()); }
        public override IDaoCrud<DepartmentsEquipment> DepartmentsEquipmentCrud { get => daoDepartmentEquipment ?? (daoDepartmentEquipment = new LinqToSqlDaoDepartmentsEquipment()); }
    }
}
