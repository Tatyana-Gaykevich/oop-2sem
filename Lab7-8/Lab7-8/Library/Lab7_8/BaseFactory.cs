﻿using Library.Model;

namespace Library.Lab7_8
{
    public abstract class BaseFactory
    {
        public static BaseFactory daoFactory = null;
        public abstract IDepartmentCrud DepartmentCrud { get; }
        public abstract IEquipmentCrud EquipmentCrud { get; }
        public abstract IDaoCrud<DepartmentsEquipment> DepartmentsEquipmentCrud { get; }

        public static BaseFactory CreateFactory(string sqlName)
        {
            switch (sqlName.ToUpper())
            {
                case "MSSQL": return daoFactory ?? (daoFactory = new MsSql.MsSqlDaoFactory());
                case "LINQTOSQL": return daoFactory ?? (daoFactory = new Linq.LinqToSqlDaoFactory());
            }
            return null;
        }
    }
}
