﻿using Library.Model;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.Lab7_8.MsSql
{
    public class MsSqlDaoDepartment : IDepartmentCrud
    {
        public Department GetById(int id)
        {
            Department department = null;
            string sql = $"SELECT * FROM departments WHERE Id={id};";

            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                SqlParameter parameter = new SqlParameter("@id", id);
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();
                
                while (reader.Read())
                {
                    department = new Department(reader.GetString(1)) { Id = reader.GetInt32(0) };
                }
            }

            return department;
        }

        public List<Department> Select()
        {
            List<Department> departments = new List<Department>();
            string sql = "SELECT * FROM departments;";

            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Department department = new Department(reader.GetString(1)) { Id = reader.GetInt32(0) };
                    departments.Add(department);
                }
            }

            return departments;
        }
    }
}