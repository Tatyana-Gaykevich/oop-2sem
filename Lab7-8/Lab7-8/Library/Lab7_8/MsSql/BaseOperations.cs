﻿using System.Data.Common;
using System.Data.SqlClient;

namespace Library.Lab7_8.MsSql
{
    internal class BaseOperations
    {
        public static void Execute(string sql)
        {
            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
            }
        }

        public static void Execute(string sql, params DbParameter[] parameters)
        {
            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();
            }
        }
    }
}
