﻿using Library.Model;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.Lab7_8.MsSql
{
    public class MsSqlDaoEquipment : IEquipmentCrud
    {
        public Equipment GetById(int id)
        {
            Equipment equipment = null;
            string sql = $"SELECT * FROM equipments WHERE Id={id};";

            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                SqlParameter parameter = new SqlParameter("@id", id);
                command.Parameters.Add(parameter);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    equipment = new Equipment(reader.GetString(1)) { Id = reader.GetInt32(0) };
                }
            }

            return equipment;
        }

        public List<Equipment> Select()
        {
            List<Equipment> equipments = new List<Equipment>();
            string sql = "SELECT * FROM equipments;";

            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Equipment equipment = new Equipment(reader.GetString(1)) { Id = reader.GetInt32(0) };
                    equipments.Add(equipment);
                }
            }

            return equipments;
        }
    }
}