﻿using Library.Model;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.Lab7_8.MsSql
{
    public class MsSqlDaoDepartmentsEquipment : IDaoCrud<DepartmentsEquipment>
    {
        private readonly MsSqlDaoDepartment SqlDaoDepartment = new MsSqlDaoDepartment();
        private readonly MsSqlDaoEquipment SqlDaoEquipment = new MsSqlDaoEquipment();

        public bool Delete(int id)
        {
            string sql = "DELETE FROM departmentsEquipment WHERE Id = @id";

            SqlParameter param = new SqlParameter("@id", id);
            BaseOperations.Execute(sql, param);

            return true;
        }

        public bool Insert(DepartmentsEquipment o)
        {
            string price = o.Price.ToString();
            price = price.Replace(',', '.');

            string sql = "INSERT departmentsEquipment (Price, Count, ResponsiblePerson, DepartmentId, EquipmentId) VALUES (@price, @count, @respPerson, @depId, @eqId);";
            
            SqlParameter[] param = new SqlParameter[5]
            {
                new SqlParameter("@price", o.Price),
                new SqlParameter("@count", o.Count),
                new SqlParameter("@respPerson", o.ResponsiblePerson),
                new SqlParameter("@depId", o.DepartmentId),
                new SqlParameter("@eqId", o.EquipmentId)
            };

            BaseOperations.Execute(sql, param);

            return true;
        }

        public List<DepartmentsEquipment> Select()
        {
            List<DepartmentsEquipment> departmentsEquipments = new List<DepartmentsEquipment>();
            string sql = "SELECT * FROM departmentsEquipment;";

            using (SqlConnection connection = new SqlConnection(DBConnection.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sql, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    DepartmentsEquipment departmentsEquipment = new DepartmentsEquipment
                    {
                        Id = reader.GetInt32(0),
                        DepartmentId = reader.GetInt32(1),
                        EquipmentId = reader.GetInt32(2),
                        ResponsiblePerson = reader.GetString(3),
                        Count = reader.GetInt32(4),
                        Price = reader.GetDecimal(5)
                    };
                    departmentsEquipment.Department = SqlDaoDepartment.GetById(departmentsEquipment.DepartmentId);
                    departmentsEquipment.Equipment = SqlDaoEquipment.GetById(departmentsEquipment.EquipmentId);
                    departmentsEquipments.Add(departmentsEquipment);
                }
            }

            return departmentsEquipments;
        }

        public bool Update(DepartmentsEquipment o)
        {
            string sql = "UPDATE departmentsEquipment SET Price = @price, Count = @count, ResponsiblePerson = @respPerson, DepartmentId=@depId, EquipmentId=@eqId WHERE Id = @id;";

            SqlParameter[] param = new SqlParameter[6]
            {
                new SqlParameter("@price", o.Price),
                new SqlParameter("@count", o.Count),
                new SqlParameter("@respPerson", o.ResponsiblePerson),
                new SqlParameter("@depId", o.DepartmentId),
                new SqlParameter("@eqId", o.EquipmentId),
                new SqlParameter("@id", o.Id),
            };

            BaseOperations.Execute(sql, param);

            return true;
        }
    }
}