﻿using Library.Model;

namespace Library.Lab7_8.MsSql
{
    public class MsSqlDaoFactory : BaseFactory
    {
        private IDepartmentCrud daoDepartment = null;
        private IEquipmentCrud daoEquipment = null;
        private IDaoCrud<DepartmentsEquipment> daoDepartmentEquipment = null;
        public override IDepartmentCrud DepartmentCrud { get => daoDepartment ?? (daoDepartment = new MsSqlDaoDepartment()); }
        public override IEquipmentCrud EquipmentCrud { get => daoEquipment ?? (daoEquipment = new MsSqlDaoEquipment()); }
        public override IDaoCrud<DepartmentsEquipment> DepartmentsEquipmentCrud { get => daoDepartmentEquipment ?? (daoDepartmentEquipment = new MsSqlDaoDepartmentsEquipment()); }
    }
}
