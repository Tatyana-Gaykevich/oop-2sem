﻿using Library.Model;
using System.Collections.Generic;

namespace Library.Lab7_8
{
    public interface IEquipmentCrud
    {
        Equipment GetById(int id);
        List<Equipment> Select();
    }
}
