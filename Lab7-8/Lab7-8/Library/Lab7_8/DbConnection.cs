﻿using System.Configuration;

namespace Library.Lab7_8
{
    /// <summary>
    /// Singleton-класс для хранения строки подключения
    /// </summary>
    public class DBConnection
    {
        private DBConnection()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["MsSqlConnectionString"].ConnectionString; // Получение строки подключения к БД из файла App.config
        }

        public static DBConnection Instance { get; } = new DBConnection();
        public string ConnectionString { get; private set; }
    }
}
