﻿using Library.Model;
using System.Collections.Generic;

namespace Library.Lab7_8
{
    public interface IDepartmentCrud
    {
        Department GetById(int id);
        List<Department> Select();
    }
}
