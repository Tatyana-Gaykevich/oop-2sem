﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Library.Lab6
{
    /// <summary>
    /// Реализация односвязанного списка.
    /// </summary>
    /// <typeparam name="T">Тип элементов.</typeparam>
    public class SuperLinkedList<T> : ICollection<T>
    {
        private Node<T> head; // первый элемент
        private Node<T> tail; // последний элемент

        public int Count { get; private set; }

        public bool IsReadOnly => false;

        /// <summary>
        /// Добавление в список.
        /// </summary>
        /// <param name="item">Добавляемый элемент</param>
        public void Add(T item)
        {
            // Проверяем входной аргумент на null.
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            // Если элемент уже есть в списке.
            if (Contains(item)) return;

            // Создаем новый элемент связного списка.
            Node<T> node = new Node<T>(item);

            // Если связный список пуст, то добавляем созданный элемент в начало,
            // иначе добавляем этот элемент как следующий за крайним элементом.
            if (head == null)
            {
                head = node;
            }
            else
            {
                tail.Next = node;
            }

            tail = node;
            Count++;
        }

        public IEnumerable<T> AsList()
        {
            T[] array = new T[Count];
            CopyTo(array, 0);
            return array.ToList();
        }

        /// <summary>
        /// Очищаем все элементы.
        /// </summary>
        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        /// <summary>
        /// Проверка на наличие элемента в списке.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return IndexOf((T)item) >= 0;
        }

        /// <summary>
        /// Копирование элементов в массив.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException(nameof(array));
            if (arrayIndex < 0) throw new ArgumentOutOfRangeException(nameof(arrayIndex));
            if (array.Length - arrayIndex < Count) throw new ArgumentException();

            // Создание вспомогательного массива.
            T[] arr = new T[array.Length];
            var current = head;

            // ЗАполнение вспомогательного массива.
            for (int i = 0; i < array.Length; i++)
            {
                arr[i] = current.Data;
                current = current.Next;
            }

            // Вызов встроенного метода для копирования данных массив.
            arr.CopyTo(array, arrayIndex);

        }

        /// <summary>
        /// Получение номера элемента.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private int IndexOf(T item)
        {
            // Проверяем входной аргумент на null.
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            // Создаем новый элемент связного списка.
            Node<T> node = new Node<T>(item);
            var current = head;
            for (int i = 0; i < Count; i++)
            {
                // Если элементы совпадают - поиск завершается.
                if (current.Equals(node)) return i;
                current = current.Next;
            }

            return -1;
        }

        /// <summary>
        /// Удаление элемента из списка.
        /// </summary>
        /// <param name="item">Образец элемента</param>
        /// <returns>true - при удачном удалении.</returns>
        public bool Remove(T item)
        {
            // Проверяем входной аргумент на null.
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            // Текущий обозреваемый элемент списка.
            var current = head;

            // Предыдущий элемент списка, перед обозреваемым.
            Node<T> previous = null;

            // Выполняем переход по всех элементам списка до его завершения,
            // или пока не будет найден элемент, который необходимо удалить.
            while (current != null)
            {
                // Если данные обозреваемого элемента совпадают с удаляемыми данными,
                // то выполняем удаление текущего элемента учитывая его положение в цепочке.
                if (current.Data.Equals(item))
                {
                    // Если элемент находится в середине или в конце списка,
                    // выкидываем текущий элемент из списка.
                    // Иначе это первый элемент списка,
                    // выкидываем первый элемент из списка.
                    if (previous != null)
                    {
                        // Устанавливаем у предыдущего элемента указатель на следующий элемент от текущего.
                        previous.Next = current.Next;

                        // Если это был последний элемент списка, 
                        // то изменяем указатель на крайний элемент списка.
                        if (current.Next == null)
                        {
                            tail = previous;
                        }
                    }
                    else
                    {
                        // Устанавливаем головной элемент следующим.
                        head = head.Next;

                        // Если список оказался пустым,
                        // то обнуляем и крайний элемент.
                        if (head == null)
                        {
                            tail = null;
                        }
                    }

                    // Элемент был удален.
                    // Уменьшаем количество элементов и выходим из цикла.
                    Count--;
                    // Если удаление успешно, то возвращаем true.
                    return true;
                }

                // Переходим к следующему элементу списка.
                previous = current;
                current = current.Next;
            }

            // Удаления не произошло.
            return false;
        }

        /// <summary>
        /// Вернуть перечислитель, выполняющий перебор всех элементов в связном списке.
        /// </summary>
        /// <returns> Перечислитель, который можно использовать для итерации по коллекции. </returns>
        public IEnumerator<T> GetEnumerator()
        {
            // Перебираем все элементы связного списка, для представления в виде коллекции элементов.
            var current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        /// <summary>
        /// Вернуть перечислитель, который осуществляет итерационный переход по связному списку.
        /// </summary>
        /// <returns> Объект IEnumerator, который используется для прохода по коллекции. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            // Просто возвращаем перечислитель, определенный выше.
            // Это необходимо для реализации интерфейса IEnumerable
            // чтобы была возможность перебирать элементы связного списка операцией foreach.
            return ((IEnumerable)this).GetEnumerator();
        }
    }
}
