﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Lab6
{
    /// <summary>
    /// Элемент спискаю
    /// </summary>
    /// <typeparam name="T">Тип элементаю</typeparam>
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }
        /// <summary>
        /// Информация.
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// Ссылка на следующий элемент.
        /// </summary>
        public Node<T> Next { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Node<T> node &&
                   EqualityComparer<T>.Default.Equals(Data, node.Data) &&
                   EqualityComparer<Node<T>>.Default.Equals(Next, node.Next);
        }

        public override int GetHashCode()
        {
            int hashCode = -890256491;
            hashCode = hashCode * -1521134295 + EqualityComparer<T>.Default.GetHashCode(Data);
            hashCode = hashCode * -1521134295 + EqualityComparer<Node<T>>.Default.GetHashCode(Next);
            return hashCode;
        }
    }
}
