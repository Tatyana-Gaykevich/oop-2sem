﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLParserLib
{
    public class Person
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string MiddleName { get; }
        public Person(string fName, string lName, string mName)
        {
            FirstName = fName;
            LastName = lName;
            MiddleName = mName;
        }
        public override string ToString()
        {
            return $"{FirstName} {LastName} {MiddleName}";
        }
    }
}
