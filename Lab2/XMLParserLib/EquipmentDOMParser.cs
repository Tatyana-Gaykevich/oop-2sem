﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace XMLParserLib
{
    public class EquipmentDOMParser : IDisposable
    {
        XmlDocument document;
        XmlReaderSettings settings;
        XmlReader reader;

        public EquipmentDOMParser(string xmlFilePath)
        {
            SetXmlSettings();
            reader = XmlReader.Create(xmlFilePath, settings);
            SetXmlDocument(reader);
        }

        public EquipmentDOMParser(byte[] byteArray)
        {
            SetXmlSettings();
            reader = XmlReader.Create(new MemoryStream(byteArray), settings);
            SetXmlDocument(reader);
        }

        private void SetXmlSettings()
        {
            XmlTextReader reader = new XmlTextReader("Equipments.xsd");
            XmlSchema schema = XmlSchema.Read(reader, ValidationCallback);
            settings = new XmlReaderSettings();
            settings.Schemas.Add(schema);
            settings.ValidationType = ValidationType.Schema;
        }

        private void SetXmlDocument(XmlReader reader)
        {
            document = new XmlDocument();
            document.Load(reader);
        }

        static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.Write("WARNING: ");
            else if (args.Severity == XmlSeverityType.Error)
                Console.Write("ERROR: ");

            Console.WriteLine(args.Message);
        }

        public List<Equipment> GetEquipments()
        {
            List<Equipment> equipments = new List<Equipment>();
            List<string> topics = new List<string>();
            string equipmentName = "", personFirstName = "", personLastName = "", personMiddleName = "";
            int equipmentAmount = 0, equipmentCost = 0;
            ControlType controlType = ControlType.ИнформационныеТехнологии;

            XmlElement root = document.DocumentElement;

            XmlNodeList subjectNodes = root.SelectNodes("Equipment");

            foreach (XmlNode equipmentNode in subjectNodes)
            {
                equipmentName = equipmentNode.SelectSingleNode("Name").InnerText;

                //XmlNode topicsNode = equipmentNode.SelectSingleNode("Topics");
                //XmlNodeList topicsList = topicsNode.SelectNodes("topic");

                //foreach (XmlNode topic in topicsList)
                //{
                //    topics.Add(topic.InnerText);
                //}

                equipmentAmount = Convert.ToInt32(equipmentNode.SelectSingleNode("AmountOfEquipment").InnerText);
                equipmentCost = Convert.ToInt32(equipmentNode.SelectSingleNode("CostOfEquipment").InnerText);

                personFirstName = equipmentNode.SelectSingleNode("//Person/FirstName").InnerText;
                personLastName = equipmentNode.SelectSingleNode("//Person/LastName").InnerText;
                personMiddleName = equipmentNode.SelectSingleNode("//Person/MiddleName").InnerText;

                Person person = new Person(personFirstName, personLastName, personMiddleName);
                equipments.Add(new Equipment(equipmentName, topics, equipmentAmount, equipmentCost, controlType, person));
            }

            return equipments;
        }

        public void WriteInFile(string filePath, string nameText, string equipmentAmountText, string equipmentCostText, 
                                string firstNameText, string lastNameText, string middleNameText, string typeOfControlText)
        {
            XmlElement equipment = document.CreateElement("Equipment");

            XmlElement name = document.CreateElement("Name");
            XmlElement equipmentAmount = document.CreateElement("AmountOfEquipment");
            XmlElement equipmentCost = document.CreateElement("CostOfEquipment");

            XmlElement person = document.CreateElement("Person");

            XmlElement firstName = document.CreateElement("FirstName");
            XmlElement lastName = document.CreateElement("LastName");
            XmlElement middleName = document.CreateElement("MiddleName");

            XmlElement typeOfControl = document.CreateElement("TypeOfControl");

            name.AppendChild(document.CreateTextNode(nameText));
            equipmentAmount.AppendChild(document.CreateTextNode(equipmentAmountText));
            equipmentCost.AppendChild(document.CreateTextNode(equipmentCostText));

            firstName.AppendChild(document.CreateTextNode(firstNameText));
            lastName.AppendChild(document.CreateTextNode(lastNameText));
            middleName.AppendChild(document.CreateTextNode(middleNameText));

            person.AppendChild(firstName);
            person.AppendChild(lastName);
            person.AppendChild(middleName);

            typeOfControl.AppendChild(document.CreateTextNode(typeOfControlText));

            equipment.AppendChild(name);
            equipment.AppendChild(equipmentAmount);
            equipment.AppendChild(equipmentCost);
            equipment.AppendChild(person);
            equipment.AppendChild(typeOfControl);

            XmlElement root = document.DocumentElement;

            root.AppendChild(equipment);
            equipment.RemoveAllAttributes();

            Dispose();
            document.Save(filePath); 
        }

        public void Dispose()
        {
            DisposeManagedResources();
        }

        protected virtual void DisposeManagedResources()
        {
            reader.Dispose();
        }
    }
}
