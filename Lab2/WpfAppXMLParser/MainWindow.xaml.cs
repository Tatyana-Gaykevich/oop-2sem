﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using XMLParserLib;
using System.Xml.Schema;

namespace WpfAppXMLParser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        EquipmentDOMParser parser;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "XML Files (*.xml)|*.xml";

                if (dlg.ShowDialog() == true)
                {
                    parser = new EquipmentDOMParser(dlg.FileName);
                    textBox.Text = File.ReadAllText(dlg.FileName);
                    textBoxFilePath.Text = dlg.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var checkIfValid = new EquipmentDOMParser(Encoding.UTF8.GetBytes(textBox.Text));

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "XML Files (*.xml)|*.xml";

                if (dlg.ShowDialog() == true)
                {
                    parser?.Dispose();
                    File.WriteAllText(dlg.FileName, textBox.Text);
                    MessageBox.Show("file saved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonCreateObjects_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Equipment> equipments = parser.GetEquipments();

                if (equipments != null && equipments.Count != 0)
                {
                    listBox.Items.Clear();

                    foreach (Equipment equipment in equipments)
                        listBox.Items.Add(equipment.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddEquipmentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string nameText = textBoxName.Text;
                string equipmentAmountText = Int32.TryParse(textBoxAmount.Text, out int resultLectures) ? textBoxAmount.Text : throw new Exception("Type must be int");
                string equipmentCostText = Int32.TryParse(textBoxCost.Text, out int resultLabs) ? textBoxCost.Text : throw new Exception("Type must be int");
                string firstNameText = textBoxLecturerFirstName.Text;
                string lastNameText = textBoxLecturerLastName.Text;
                string middleNameText = textBoxLecturerMiddleName.Text;
                string typeOfControlText = comboBoxControlType.Text;

                parser.WriteInFile(textBoxFilePath.Text, nameText, equipmentAmountText, equipmentCostText,
                               firstNameText, lastNameText, middleNameText, typeOfControlText);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
