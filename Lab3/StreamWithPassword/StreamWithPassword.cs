﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Remoting;
using System.Threading;

namespace StreamWithPasword
{
    public class StreamWithPassword : Stream
    {
        private Stream stream;
        private string pasword;
        public bool PaswordEntered { get; private set; }
        public override bool CanRead { get { return stream.CanRead; } }

        public override bool CanSeek { get { return stream.CanSeek; } }

        public override bool CanWrite { get { return stream.CanWrite; } }

        public override long Length { get { return stream.Length; } }

        public override long Position { get { return stream.Position; } set { stream.Position = value; } }

        public StreamWithPassword(Stream stream, string pasword)
        {
            this.stream = stream;
            this.pasword = pasword;
            PaswordEntered = false;
        }

        public void CheckPasword(Stream stream)
        {
            stream.Seek(0, SeekOrigin.Begin);
            Byte[] pasword = new Byte[this.pasword.Length];
            stream.Read(pasword, 0, this.pasword.Length);
            string checkPassword = "";
            for (int i = 0; i < pasword.Length; i++)
            {
                checkPassword += Convert.ToChar(pasword[i]);
            }
            if (string.Equals(this.pasword, checkPassword))
            {
                PaswordEntered = true;
            }
        }

        public override void Flush()
        {
            if (PaswordEntered) stream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (PaswordEntered) return stream.Read(buffer, offset, count);
            else return -1;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (PaswordEntered) return stream.Seek(offset, origin);
            else return -1;
        }

        public override void SetLength(long value)
        {
            if (PaswordEntered) stream.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (PaswordEntered) stream.Write(buffer, offset, count);
        }
    }
}
