﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using StreamWithPasword;
using Microsoft.Win32;

namespace Lab3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private FileStream file = null;
        private BufferedStream buffer = null;
        private StreamWithPassword stream = null;
        private StreamReader reader = null;
        private MemoryStream memory = null; // Создает поток, резервным хранилищем которого является память.
        private Encoding encoding = Encoding.UTF8;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonRead_Click(object sender, RoutedEventArgs e)
        {
            if (stream != null && stream.PaswordEntered)
            {
                eriteTextBoxs.Text += reader.ReadLine() + "\n";

            }
        }

        private void buttonEnter_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if ((bool)openFileDialog.ShowDialog())
            {
                if (reader != null) reader.Dispose();
                if (stream != null) reader.Dispose();
                if (buffer != null) reader.Dispose();
                if (file != null) reader.Dispose();
                file = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read);
                buffer = new BufferedStream(file);
                stream = new StreamWithPassword(buffer, paswordBox.Text);
                reader = new StreamReader(stream);
                info.Content = new ContentControl().Tag = "Неверный пароль";
                paswordBox.Text = "";
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (stream != null)
            {
                byte[] textPassword = encoding.GetBytes(paswordBox.Text);
                memory = new MemoryStream();
                memory.Write(textPassword, 0, textPassword.Length);
                stream.CheckPasword(memory);
                if (stream.PaswordEntered)
                {
                    info.Content = new ContentControl().Tag = "Верный пароль";
                }
                else
                {
                    info.Content = new ContentControl().Tag = "Неверный пароль";
                }
                paswordBox.Text = "";
                memory.Dispose();
            }
        }
    }
}
