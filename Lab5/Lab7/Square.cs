﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab7
{
    public class Square:Rectangle
    {
        
        private Point rightConer;

        public event EventHandler Click;
        public Square(Point leftConer, int size, Color color, Control parent):base(leftConer,new Point(leftConer.X+size,leftConer.Y+size),color,parent)
        {
            Parent.MouseClick += OnParentMouseClick;
        }

        private void OnParentMouseClick(object sender, MouseEventArgs args)
        {
            if ((args.X > LeftConer.X && args.X < RightConer.X && args.Y > LeftConer.Y && args.Y < RightConer.Y))
            {               
                Click(this, EventArgs.Empty);
            }
        }

        public override Point RightConer
        {
            get
            {
                return rightConer;
            }
            set
            {
                if(value.X-LeftConer.X!=value.Y-LeftConer.Y)
                    throw new Exception("Неправильный квадрат");
                rightConer = value;
            }
        }
    }
}
