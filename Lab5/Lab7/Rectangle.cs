﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab7
{
    public class Rectangle
    {
        
        private bool isMouseEnter = false; 

        public Point LeftConer { get; set; }
        virtual public Point RightConer { get; set; }
        public Color Color { get; set; }
        public bool IsDraw { get; set; }

        public Control Parent { get; set; }

        public event EventHandler MouseEnter;
        public event EventHandler MouseLeave;

        public Rectangle(Point leftConer, Point rightConer, Color color, Control parent)
        {
            LeftConer = leftConer;
            RightConer = rightConer;
            Color = color;
            Parent = parent;

            Parent.MouseMove += OnParentMouseMove;

        }
        public  void Paint()
        {
            if (IsDraw)
            {
                Graphics graphics = Parent.CreateGraphics();
                Brush brush = new SolidBrush(Color);
                graphics.FillRectangle(brush, LeftConer.X, LeftConer.Y, RightConer.X-LeftConer.X, RightConer.Y-LeftConer.Y);
            }
           
        }
        private void OnParentMouseMove(object sender,MouseEventArgs args)
        {
            if(!isMouseEnter)
            {
                if ((args.X > LeftConer.X && args.X < RightConer.X && args.Y > LeftConer.Y && args.Y < RightConer.Y))
                {
                    isMouseEnter = true;
                    MouseEnter(this, EventArgs.Empty);
                }
            }
            else
            {
                if ((args.X < LeftConer.X || args.X > RightConer.X || args.Y < LeftConer.Y || args.Y > RightConer.Y))
                {
                    isMouseEnter = false;
                    MouseLeave(this, EventArgs.Empty);
                }
            }
        }
        
        
    }
}
