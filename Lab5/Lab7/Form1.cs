﻿using System;
using System.Drawing;
using System.Media;
using System.Windows.Forms;

namespace Lab7
{
    public partial class Form1 : Form
    {
        Rectangle rectangle;
        Square square;
        public Form1()
        {
            InitializeComponent();
            rectangle
                = new Rectangle(new Point(10, 10), new Point(100, 50), Color.Blue,panel1);
            rectangle.MouseEnter += Rectangle_MouseEnter;
            
            rectangle.MouseLeave += (r, e) =>
            {
                Rectangle rec = (Rectangle)r;
                rec.Color = Color.Blue;
                rec.Paint();
            };
            square = new Square(new Point(100, 100), 200, Color.Aqua, panel1);   
            square.MouseEnter += (r, e) =>
            {
                Rectangle rec = (Rectangle)r;
                rec.Color = Color.Blue;
                rec.Paint();
            };
            square.MouseLeave += (r, e) =>
            {
                Rectangle rec = (Rectangle)r;
                rec.Color = Color.Aqua;
                rec.Paint();
            };
            square.Click += (r, e) => { SystemSounds.Beep.Play(); };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rectangle.IsDraw = true;
            rectangle.Paint();
        }

        private void Rectangle_MouseEnter(object sender, EventArgs e)
        {
            Rectangle r = (Rectangle)sender;
            r.Color=Color.Red;
            r.Paint();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            square.IsDraw = true;
            square.Paint();
        }
    }
}
