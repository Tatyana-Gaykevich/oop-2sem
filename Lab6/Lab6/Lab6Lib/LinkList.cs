﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Lib
{
    public class LinkList<T> : ICollection<T>
    {
        private Node<T> head;  // первый элемент
        private Node<T> tail;  // последний элемент

        public int Count { get; private set; } // количество элементов в списке
        public bool IsReadOnly => false;

        // добавление элемента
        public void Add(T item)
        {
            Node<T> node = new Node<T>(item);

            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;

            Count++;
        }

        // очистка списка   
        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        // содержит ли список элемент
        public bool Contains(T item)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(item))
                    return true;
                current = current.Next;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            foreach (T item in this)
            {
                array[arrayIndex] = item;
                arrayIndex++;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public bool Remove(T item)
        {
            Node<T> current = head;
            Node<T> previous = null;

            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null) tail = previous;
                    }
                    else
                    {
                        head = head.Next;
                        if (head == null) tail = null;
                    }
                    Count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
