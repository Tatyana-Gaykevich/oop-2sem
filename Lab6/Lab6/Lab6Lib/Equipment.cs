﻿using System.Collections.Generic;

namespace Lab6Lib
{
    public enum ControlType
    {
        ИнформационныеТехнологии,
        ФизическоеВоспитание,
        Экономика,
        ПромышленнаяЭлектроника
    }

    public class Equipment
    {
        public string Name { get; set; } // Название оборудования
        public string Person { get; set; } // Материально ответственное лицо

        //private string topicsString;

        public int AmountOfEquipment { get; set; } // Количество оборудования
        public int CostOfEquipment { get; set; } // Стоимость оборудования
        public ControlType TypeOfControl { get; set; } // Кафедра
        //public Person Person { get; set; } // Материально ответственное лицо


        public Equipment(string name=null, string person = null, int costOfEquipment=0,
                       int amountOfEquipment=0, ControlType typeOfControl = ControlType.Экономика/*, Person person=null*/)
        {
            //Topics = topics;
            Name = name;
            Person = person;
            
            AmountOfEquipment = amountOfEquipment;
            CostOfEquipment = costOfEquipment;
            TypeOfControl = typeOfControl;
            //  Person = person;

            //foreach (string s in topics)
            //    topicsString += s /*+ "\n "*/;
        }
        public override string ToString()
        {
            return $"Назв. оборудования: {Name} " +
                $"Кол-во: {AmountOfEquipment}, " +
                $"Стоимость (ед): {CostOfEquipment}, " +
                $"Кафедра: {TypeOfControl.ToString()}, " +
                $"Ответственное лицо: {Person} \n";
              //  $"Ответственное лицо: {Person.ToString()}";
        }
    }
}
