﻿using System;
using System.Linq;
using System.Windows.Forms;
using Lab6Lib;


namespace WinFormLab6
{
    public partial class Form1 : Form
    {
        private LinkList<Equipment> equipments;
        private Equipment selectEquipment;

        public Form1()
        {
            InitializeComponent();
            equipments = new LinkList<Equipment>();
            comboBoxControlType.DataSource = Enum.GetNames(typeof(ControlType));
        }

        private Equipment GetEquipment()
        {
            Equipment equipment = new Equipment();
            equipment.Name = textBoxName.Text;
            equipment.AmountOfEquipment = int.Parse(textBoxAmount.Text);
            equipment.CostOfEquipment = int.Parse(textBoxCost.Text);
            equipment.TypeOfControl = (ControlType)Enum.Parse(typeof(ControlType), comboBoxControlType.SelectedItem.ToString());
            equipment.Person = textBoxPerson.Text;
            return equipment;
        }

        //private void UpdateListBox(int index)
        //{
        //    listBox.Items.Clear();
        //    foreach (Equipment equipment in equipments)
        //    {
        //        listBox.Items.Add(equipment.Name + " " 
        //                        + equipment.AmountOfEquipment + " "
        //                        + equipment.CostOfEquipment + " "
        //                        + equipment.Person + " "
        //                        + equipment.TypeOfControl.ToString());
        //    }
        //    listBox.SelectedIndex = index;
        //}

        //private Equipment GetEquipmentByIndex(int index)
        //{
        //    //Equipment[] array = new Equipment[equipments.Count];
        //    //equipments.CopyTo(array, 0);

        //    //return array[index];
        //    var list = equipments.ToList();
        //    return list[index];
        //}

        private void UpdateSelectEquipment()
        {
            textBoxName.Text = selectEquipment.Name;
            comboBoxControlType.SelectedIndex= (int)selectEquipment.TypeOfControl;
            textBoxPerson.Text = selectEquipment.Person;
            textBoxCost.Text = selectEquipment.CostOfEquipment.ToString();
            textBoxAmount.Text = selectEquipment.AmountOfEquipment.ToString();
        }

        private void ButtonAdd_Click_Click(object sender, EventArgs e)
        {
            selectEquipment = GetEquipment();
            equipments.Add(selectEquipment);
            listBox.Items.Add(selectEquipment);
            listBox.SelectedIndex = equipments.Count - 1;
        }

        private void buttonDel_Click_Click(object sender, EventArgs e)
        {
            if (equipments.Count == 0) return;
            selectEquipment = listBox.SelectedItem as Equipment;
            equipments.Remove(selectEquipment);
            listBox.Items.Remove(selectEquipment);

        }

       


        //private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    selectEquipment = listBox.SelectedItem as Equipment;
        //    UpdateSelectEquipment();
        //}
    }
}
