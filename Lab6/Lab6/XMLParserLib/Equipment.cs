﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLParserLib
{
    public enum ControlType
    {
        ИнформационныеТехнологии,
        ФизическоеВоспитание,
        Экономика,
        ПромышленнаяЭлектроника
    }

    public class Equipment
    {
        public string Name { get; } // Название оборудования

        private string topicsString;

        public List<string> Topics { get; }
        public int AmountOfEquipment { get; } // Количество оборудования
        public int CostOfEquipment { get; } // Стоимость оборудования
        public ControlType TypeOfControl { get; } // Кафедра
        public Person Person { get; } // Материально ответственное лицо

      
        public Equipment(string name, List<string> topics, int costOfEquipment,
                       int amountOfEquipment, ControlType typeOfControl, Person person)
        {
            Name = name;
            Topics = topics;
            AmountOfEquipment = amountOfEquipment;
            CostOfEquipment = costOfEquipment;
            TypeOfControl = typeOfControl;
            Person = person;

            foreach (string s in topics)
                topicsString += s + "\n ";
        }
        public override string ToString()
        {
            return $"Название оборудования: {Name} \n" +
                $"Количество: {AmountOfEquipment}, " +
                $"Стоимость (ед): {CostOfEquipment}, " +
                $"Кафедра: {TypeOfControl.ToString()}, " +
                $"Ответственное лицо: {Person.ToString()}";
        }
    }
}
